// console.log("Last session of the week!")

// [section] While loop

// A While loop takes in an expression/condition.
// Expressions are any unit of code that can be evaluated to a  value.
// if the condition evalutes to be true the statements inside the code block will be executed.
// A loop will iterate a certain number of times until an expression/ condition is met.
// Iteration is the term given to the repitition of statements

/*
	Syntax:
	while(expression/condition){
		statements;
		iteration;
	}

*/

/*let count = 5;
// while the value of count is not equal to zero
while(count!==0){
	// The current value of count is printed out.
	console.log("While:" +count);
	// decrement
	count --;
	console.log("Value of count after the Iteration:" +count);
	// Decreases the value of count by 1 every iteration to stop the loop when it reaches 0.
	// loops occupy a significant amount of memory space in our devicess.
	// make sure that expressions/conditions in loops have thier corresponding increment/decrement operators to stop the loop
	// forgetting to include this in loops will make our applicationsrun an infinite loop.
	// After running the script, if a slowresponse from the browser us esperience or an infinite loop is see seen in the console quickly close the application/tab/browser to avoid this.
}*/

//[section] do while Loop
/*
	- a do-wile loop works alot like while loop. but unlike while loops, do-while loops guaratntee that the code will be executed ate least once.

	Syntax:
	do{
	statement:
	iteration;
	}
	while(expression/condition)
*/

/*let number = Number(prompt("Give me a number."));
do{
	console.log("Do While: "+ number);
	number++;
}
while(number < 10);*/

// [section] For loop

/*
	-A for loop is more flexibe than while and do-while.
	It consists of 3 parts:
	1. The "initialization" value that will track the progression of the loop.
	2. Th "expression/condition" that will be evaluated which will determine wether the loop will run one more time.
	3. The "finalExpression" indicates how to advance the loop.

	Syntax:
	for(initialization; expression/condition; finalExpression){
		statement/statements;
	}
*/

/*
	- we will create a loop that will start from 0 and end 20
	- Every iteration of the loop, the value of count will be checked ifit is equal or less than 20
	-if the value of count is lexx than or equal to 20 the statement inside of the loopwill execute.
	-the value of count will be incremented by one for each iteration.
*/
/* if let mas higher sa count = 20 u can use count--*/
/*for (let count = 0; count <= 20 ; count++){
	console.log("The current value of count is: " +count);

}*/

// let myString = "alex";
	// character in strings may be count using the length property
	// strings are the special compare to other data types, that it has access to functins and other pieces of information.

// console.log(myString.length);
	/*Access character of a string.*/
	/*Individual characters of a string may be accesses using it's index number.*/
	/*The first character in a string corresponds to 0, the next is 1 up to the nth/last character.*/
/*console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);*/

/*for (let i=0; i < myString.length; i+=2){
	console.log(myString[i]);
}

let numA = 15;

for (let i=0; i <=10; i++){
	let exponential = numA **i; 

	console.log(exponential);
}
*/


/*Create a string named "myName* with value of "Alex" */

let myName = "Alex";

/*Create a loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is vowel*/

for(let i = 0; i < myName.length; i++){
		if(myName[i].toLowerCase() === "a" ||
			myName[i].toLowerCase() === "e" ||
			myName[i].toLowerCase() === "i" ||
			myName[i].toLowerCase() === "o" ||
			myName[i].toLowerCase() === "u"){

			console.log(3);
		}
		else{
			console.log(myName[i]);
		}
	}



//[section] Continue and break statements

	/*
		- The continue statements allows the code to go to the next iteration of the loop witout finishing the execution of all statements in a code block
		- The "break" statement is used to terminate the current loop once a match has been found.
	*/

// Create a loop that if the count value is divided by 2 and the remainder is 0,it will print the number and continue to the next iteration of the loop

/*for(let count = 0; count<=20; i++){

}*/

	/*How this For Loop works:*/
	/*
		1. The loop will start at 0 for the the value of "count".
		2. It will check if "count" is less than the or equal to 20.
		3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
		4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
		5. If the value of count is not equal to 0, the console will print the value of "count".
		6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
		7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
		8. The value of "count" will be incremented by 1 (e.g. count = 1)
		9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement

	*/

	for(let count = 0; count <=20; count++){
		// if reminder is equal to 0
			if(count % 2 === 0){
				/*It tells the code to continue to the next iteration of the loop*/
				/*This ignores all statements located after the continue statements*/
				continue;
			}
				else if (count>10) {
				/*Tells the code to terminate/stop the loop even if the expression /condition of the loop defines that it should execute so long as the value of count is less than */
				break;

			}
				console.log("continue and break: "+count);
} 

let name = "alexandro";
	for (let i = 0; i < name.length; i++ ){
		if(name[i].toLowerCase() === "a"){
			console.log("continue to next iteration.")
			continue;
		}

		if(name[i].toLowerCase() === "d"){
			console.log("console log before the break!");
			break;
		}
	console.log(name[i])
	}